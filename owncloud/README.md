# Owncloud
## Description
Creates an [Owncloud](https://owncloud.org/) instance which can authenticate users against an imap server.

## Dependencies
- server
- ssmtp
- nginx
- php
- postgresql
- ldapclient

## Variables
### owncloud_appstore_enabled
- default: true

Whether the owncloud [appstore](https://apps.owncloud.com/) is enabled.

### owncloud_max_filesize
- default: '50G'

Maximal uploadsize for the owncloud instance.

### owncloud_use_imap_auth
- default: false

Whether to use imap authentication.

### owncloud_dbpassword
The password used to authenticate to the owncloud database.

### owncloud_admin_name
The admin username. Only used at setup and upgrade time.

### owncloud_admin_password
The admin password. Only used at setup and upgrade time.

### owncloud_auth_imap_connection
The imap host to authenticate against, eg `'mail.vanwa.ch:143/imap/tls'`

### owncloud_smtp_from_name
Name part for the from field in notification emails.

### owncloud_smtp_from_domain
Domain part for the from field in notification emails.

### owncloud_smtp_auth_type
- default: PLAIN  

Authentification type for the smtp relay.

One of:
- NONE
- PLAIN
- LOGIN
- NTLM

### owncloud_smtp_security
- default: TLS

Security for the smtp relay.

One of:
- NONE
- SSL
- TLS

### owncloud_smtp_host
Smtp relay to conenct to.

### owncloud_smtp_port
Port the smtp relay listens on.

### owncloud_smtp_user
Username to authenticate to the smtp relay.

### owncloud_smtp_password
Password to authenticate to the smtp relay.

### owncloud_domain: 'cloud.vanwa.ch'
Domain where the owncloud instance can be reached.

### owncloud_apps
- default: []

A list of owncloud apps to activate.

Eg:
```
owncloud_apps:
    - 'news'
    - 'mail'
```

### owncloud_ldap_password

The password used to connect to the ldap server.
