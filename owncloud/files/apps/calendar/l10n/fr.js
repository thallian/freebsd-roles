OC.L10N.register(
    "calendar",
    {
    "Calendar" : "Agenda",
    "New event" : "Nouvel événement",
    "Global" : "Global",
    "None" : "Aucun",
    "Attendees" : "Participants",
    "Reminders" : "Rappels",
    "Does not repeat" : "Pas de répétition",
    "Daily" : "Quotidien",
    "Weekly" : "Hebdomadaire",
    "Every Weekday" : "En semaine",
    "Bi-weekly" : "Bihebdomadaire",
    "Monthly" : "Mensuel",
    "Yearly" : "Annuel",
    "never" : "jamais",
    "by occurances" : "par occurrences",
    "by date" : "par date",
    "by monthday" : "par jour du mois",
    "by weekday" : "par jour de la semaine",
    "by events date" : "par date d’événement",
    "by yearday(s)" : "par jour(s) de l'année",
    "by week no(s)" : "par numéro(s) de semaine(s)",
    "by day and month" : "par jour et mois",
    "Monday" : "Lundi",
    "Tuesday" : "Mardi",
    "Wednesday" : "Mercredi",
    "Thursday" : "Jeudi",
    "Friday" : "Vendredi",
    "Saturday" : "Samedi",
    "Sunday" : "Dimanche",
    "Other" : "Autre",
    "Individual" : "Individuel",
    "Group" : "Groupe",
    "Resource" : "Ressource",
    "Room" : "Lieu",
    "Unknown" : "Inconnu",
    "Required" : "Requis",
    "Optional" : "Optionnel",
    "Does not attend" : "Ne participera pas",
    "When shared show full event" : "Afficher l'événement complet en cas de partage",
    "When shared show only busy" : "Afficher seulement la plage horaire occupée en cas de partage",
    "When shared hide this event" : "Masquer cet événement en cas de partage",
    "At time of event" : "À l'heure de l'événement",
    "5 minutes before" : "5 minutes avant",
    "10 minutes before" : "10 minutes avant",
    "15 minutes before" : "15 minutes avant",
    "30 minutes before" : "30 minutes avant",
    "1 hour before" : "1 heure avant",
    "2 hours before" : "2 heures avant",
    "Custom" : "Personnalisé",
    "Audio" : "Son",
    "E Mail" : "Courriel",
    "Pop up" : "Fenêtre d'alerte",
    "sec" : "sec",
    "min" : "min",
    "hours" : "heures",
    "days" : "jours",
    "week" : "semaine",
    "Before" : "Avant",
    "After" : "Après",
    "Start" : "Début",
    "End" : "Fin",
    "Week {number} of {year}" : "Semaine {number} de {year}",
    "Successfully imported" : "Importé avec succès",
    "Partially imported, 1 failure" : "Partiellement importé, 1 échec",
    "Partially imported, {n} failures" : "Partiellement importé, {n} échecs",
    "Audio alarm" : "Alerte audio",
    "Pop-up" : "Fenêtre d'alerte",
    "E-Mail" : "Courriel",
    "{type} {time} before the event starts" : "{type} {time} avant l'événement",
    "{type} {time} before the event ends" : "{type} {time} avant la fin de l'événement",
    "{type} {time} after the event starts" : "{type} {time} après le début de l'événement",
    "{type} {time} after the event ends" : "{type} {time} après la fin de l'événement",
    "{type} at the event's start" : "{type} au début de l'événement",
    "{type} at the event's end" : "{type} à la fin de l'événement",
    "{type} at {time}" : "{type} à {time}",
    "{calendar} shared by {owner}" : "{calendar} partagé par {owner}",
    "Day" : "Jour",
    "Week" : "Semaine",
    "Month" : "Mois",
    "Today" : "Aujourd'hui",
    "Share Calendar" : "Partager l'agenda",
    "group" : "groupe",
    "New Calendar" : "Nouvel agenda",
    "Create" : "Créer",
    "Subscriptions" : "Abonnements",
    "New Subscription" : "Nouvel abonnement",
    "Url" : "URL",
    "Edit event" : "Modifier l'événement",
    "Delete Event" : "Supprimer l'événement",
    "Save Event" : "Enregistrer l'événement",
    "Time" : "Heure",
    "Type" : "Type",
    "Relative" : "Relatif",
    "Absolute" : "Absolu",
    "Date" : "Date",
    "Repeat" : "Répéter",
    "times every" : "Répéter tous les",
    "Add" : "Ajouter",
    "Please add your email address in the personal settings in order to add attendees." : "Veuillez ajouter votre adresse mail dans les paramètres du comptes  dans l'ordre pour ajouter des participants .",
    "Title of the Event" : "Titre de l'événement",
    "from" : "du",
    "to" : "à",
    "All day Event" : "Journée entière",
    "Location" : "Emplacement",
    "starts" : "démarre",
    "ends" : "termine",
    "Delete" : "Supprimer",
    "Cancel" : "Annuler",
    "More ..." : "Plus…",
    "Update" : "Mettre à jour",
    "Close" : "Fermer",
    "Interval" : "Intervalle",
    "Description" : "Description",
    "Export" : "Exporter",
    "Import Calendars" : "Importer des agendas",
    "Import canceled" : "Importation annulée",
    "Analyzing calendar" : "Analyse de l'agenda en cours",
    "The file contains objects incompatible with the selected calendar" : "Le fichier contient des objets incompatibles avec l'agenda sélectionné",
    "New calendar" : "Nouvel agenda",
    "Import scheduled" : "Importation planifiée",
    "Settings" : "Paramètres",
    "Import calendar" : "Importer un agenda",
    "No Calendars selected for import" : "Aucun agenda sélectionné pour import",
    "Primary CalDAV address" : "Adresse CalDAV principale",
    "iOS/OS X CalDAV address" : "Adresse CalDAV pour iOS/OS X"
},
"nplurals=2; plural=(n > 1);");
