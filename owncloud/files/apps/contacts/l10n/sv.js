OC.L10N.register(
    "contacts",
    {
    "Contacts" : "Kontakter",
    "No contacts in here" : "Det finns inga kontakter här",
    "Name" : "Namn",
    "Add contact" : "Lägg till en kontakt",
    "All contacts" : "Alla kontakter",
    "City" : "Stad",
    "State or province" : "Stat eller provins",
    "Country" : "Land",
    "Address" : "Adress",
    "{addressbook} shared by {owner}" : "{addressbook} delad av {owner}",
    "Nickname" : "Smeknamn",
    "Organization" : "Organisation",
    "Notes" : "Anteckningar",
    "Website" : "Webbplats",
    "Title" : "Rubrik",
    "Role" : "Roll",
    "Home" : "Hem",
    "Work" : "Arbete",
    "Other" : "Annat",
    "Groups" : "Grupper",
    "Birthday" : "Födelsedag",
    "Email" : "E-post",
    "Instant messaging" : "Snabbmeddelanden",
    "Phone" : "Telefon",
    "Mobile" : "Mobil",
    "Fax" : "Fax",
    "Pager" : "Personsökare",
    "Voice" : "Röst"
},
"nplurals=2; plural=(n != 1);");
