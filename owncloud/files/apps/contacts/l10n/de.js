OC.L10N.register(
    "contacts",
    {
    "Contacts" : "Kontakte",
    "No contacts in here" : "Keine Kontakte gefunden",
    "Name" : "Name",
    "Add field ..." : "Feld hinzufügen ...",
    "Add contact" : "Kontakt hinzufügen",
    "All contacts" : "Alle Kontakte",
    "Post Office Box" : "Briefkasten",
    "Postal Code" : "Postleitzahl",
    "City" : "Stadt",
    "State or province" : "Staat oder Provinz",
    "Country" : "Land",
    "Address" : "Adresse",
    "{addressbook} shared by {owner}" : "{addressbook} geteilt von {owner}",
    "Nickname" : "Spitzname",
    "Organization" : "Organisation",
    "Notes" : "Notizen",
    "Website" : "Website",
    "Title" : "Titel",
    "Role" : "Rolle",
    "Federated Cloud ID" : "Federated-Cloud-ID",
    "Home" : "Home",
    "Work" : "Arbeit",
    "Other" : "Andere",
    "Groups" : "Gruppen",
    "Birthday" : "Geburtstag",
    "Email" : "E-Mail",
    "Instant messaging" : "Instant Messaging",
    "Phone" : "Telefon",
    "Mobile" : "Mobil",
    "Fax" : "Fax",
    "Fax home" : "Fax persönlich",
    "Fax work" : "Fax geschäftlich",
    "Pager" : "Pager",
    "Voice" : "Anruf"
},
"nplurals=2; plural=(n != 1);");
