OC.L10N.register(
    "contacts",
    {
    "Contacts" : "Επαφές",
    "No contacts in here" : "Δεν υπάρχουν επαφές εδώ",
    "Name" : "Όνομα",
    "Add contact" : "Προσθήκη επαφής",
    "All contacts" : "Όλες οι επαφές",
    "City" : "Πόλη",
    "State or province" : "Νομός ή περιφέρεια",
    "Country" : "Χώρα",
    "Address" : "Διεύθυνση",
    "{addressbook} shared by {owner}" : "Το {addressbook} διαμοιράστηκε από τον/την {owner}",
    "Nickname" : "Παρατσούκλι",
    "Organization" : "Οργανισμός",
    "Notes" : "Σημειώσεις",
    "Website" : "Ιστοσελίδα",
    "Title" : "Τίτλος",
    "Federated Cloud ID" : "Federated Cloud ID",
    "Home" : "Σπίτι",
    "Work" : "Εργασία",
    "Other" : "Άλλο",
    "Groups" : "Ομάδες",
    "Birthday" : "Γενέθλια",
    "Email" : "Ηλ. ταχυδρομείο",
    "Instant messaging" : "Άμεσα μηνύματα",
    "Phone" : "Τηλέφωνο",
    "Mobile" : "Κινητό",
    "Fax" : "Φαξ",
    "Pager" : "Βομβητής",
    "Voice" : "Ομιλία"
},
"nplurals=2; plural=(n != 1);");
