# Let's Encrypt
## Description

Creates a container for [Let's Encrypt](https://letsencrypt.org/) provisioning using [acme-tiny](https://github.com/diafygi/acme-tiny).

It generates and requests missing private keys, csrs and certificates and updates the stuff every two months.
Mount the respective directories into the jails that need them.

The nginx server redirects every request that is not an acme challenge to the respective https site.

## Dependencies
- server
- ssmtp
- nginx

## Variables
### letsencrypt_domains
- default: []

List of domains for which to request certificates with the following structure:

- namespace: name of the subfolder where the files are written to
- name: the domain, eg `mac.beachballing.com`
