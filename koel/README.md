# Koel
## Description
Sets up a [Koel](http://koel.phanan.net/) instance.

## Dependencies
- server
- ssmtp
- nginx
- php
- postgresql

## Variables
### koel_debug
- default: false

Prints detailed stacktraces if true.

### koel_app_key

For encryption. Set this to some big random string.

### koel_jwt_secret

Secret for JWT. Must be 32 chars in length.

### koel_admin_email

Email address for the initial admin account.

### koel_admin_name

Name of the initial admin.

### koel_admin_password

Password of the initial admin.

### koel_max_scan_time
- default: 1000

Maximum scan time, in seconds.

### koel_dbpassword

Password for the koel database user.

### koel_smtp_host

Smtp host to send emails.

### koel_smtp_port
- default: 587

Port to connect to on the smtp host.

### koel_smtp_user

The username with which to authenticate to the smtp server.

### koel_smtp_password

The password for the smtp user.

### koel_smtp_encryption
- default: 'tls'

What kind of encryption to use with the smtp host.

### koel_output_bit_rate
- default: 128

The bit rate of the out mp3 stream. Higher value results in better quality but bigger files and more bandwidth.
