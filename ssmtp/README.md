# SSMTP
## Description
Replaces sendmail on a host with [ssmtp](http://linux.die.net/man/8/ssmtp) for sending mail over an smtp relay.

## Dependencies
- server

## Variables
### ssmtp_mail_use_tls
- default: 'YES'

Whether to use TLS to connect to the smtp relay.

### ssmtp_mail_use_starttls
- default: 'YES'

Whether to use STARTTLS to connect to the smtp relay.

### ssmtp_mail_auth_method
- default: 'LOGIN'

Which smtp authentication method to use.

### ssmtp_aliases
- default: []

Mapping between local accounts and outgoing mail with the following structure:

- local: the local name
- from: the outgoing address

### ssmtp_mail_relay
The smtp relay to connect to.

### ssmtp_mail_hostname
Where the mail will seem to come from.

### ssmtp_mail_user
Username to authenticate to the smtp relay.

### ssmtp_mail_password
Password to authenticate to the smtp relay

### ssmtp_master_mail
The mail address where all is sent to.
