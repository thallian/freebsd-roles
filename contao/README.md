# Contao
## Description
Creates a [Contao](http://www.postgresql.org/) cms instance.

## Dependencies
- server
- ssmtp
- monit
- mariadb
- nginx
- php

## Variables

### contao_dbpassword

Password for the contao database.

### contao_max_filesize
- default: 32M

The maximal filesize for uploads.

### contao_version
- default 4.1.3

The contao version to be used.

### contao_domain

Domain where the contao instance can be reached.

### contao_mail_host

The host used to send mails.

### contao_mail_user

User to authenticate to the mail host.

### contao_mail_password

Password of the mail user.

### contao_mail_port
- default: 587

Port to use with the mail host.

### contao_mail_encryption
- default: 'tls'

Encryption method to use with the mail host.

One of:

- tls
- ssl
- null

### contao_secret
Secret key for contao.

### contao_git_url
- default: 'https://github.com/contao/standard-edition'

Contao git repository to use.

### contao_install_password
Installation password for a new installation.
