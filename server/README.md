# Server
## Description
A role for some basic stuff on a host, be it jail or bare metal.

Every role has this as a dependency.

Note that the dictionary keys do not matter, the only need to be different between roles..

## Dependencies
- server

## Variables
### server_hosts
- default: {}

Dictionary of hostfile lines with the following object structure:
- ip: The ip to use for the domain.
- domain: The mapped domain.

### server_pkgs
- default: {}

Dictionary of binary packages that have to be installed.

### server_ports
- default: {}

Dictionary of ports that have to be installed with the following structure:

- port: the portname, eg `www/owncloud`
- with: options to build the port with
- without: options to build the port without

### server_users
- default: {}

Dictionary of users that have to be present in the jail with the following structure:

- user: username
- group: groupname
- home: the user's home directory
- generate_ssh_key: whether to generate an ssh key (defaults to no)
- shell: the users shell (defaults to '/bin/csh')

### server_allowed_ssh_keys
- default: ''

Newline seperated allowed ssh keys for the root account. Only usable on jails as root
authentication is disabled on the jailhost.

### server_timezone
The timezone this host uses.

### server_sftp_chroot

Base path for the sftp chroot. Must be owned by root.

### server_sftp_access
- default: []

List of objects with the following structure:

- name: name of the user to get access via sftp
- path: path to get access to

### server_pkg_url
- default: 'http://pkg.freeBSD.org/${ABI}/latest'

The pkg repo url.
