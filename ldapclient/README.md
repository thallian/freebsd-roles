# OpenLDAP

## Description
Sets up an openldap client [OpenLDAP](http://www.openldap.org/) in a jail.

## Dependencies
- server

## Variables
### ldapclient_sizelimit
- default: 500

Specifies a size limit (number of entries) to use when performing searches.

### ldapclient_timelimit
- default: 3600

Specifies a time limit (in seconds) to use when performing searches.

### ldapclient_deref
- default: 'never'  

Specifies how alias dereferencing is done when performing a search.

One of the following:

- never: aliases are never dereferenced.
- searching: aliases are only dereferenced when locating the base object of the search.
- always: aliases are dereferenced both in searching and in locating the base object of the search.

### ldapclient_uri

Ldap uris to connect to, eg: `ldap://acc.example.com`

### ldapclient_base_dn

The base dn of the ldap server, eg: `dc=example,dc=com`
