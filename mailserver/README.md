# Mailserver
## Description
Sets up a complete mailserver solution, consisting of:
- [Postfix](http://www.postfix.org/)
- [Dovecot](http://www.dovecot.org/)
- [Roundcube](https://roundcube.net/)
- [SPF](https://en.wikipedia.org/wiki/Sender_Policy_Framework) (only signing, no checking)
- [DKIM](https://en.wikipedia.org/wiki/DomainKeys_Identified_Mail) via [opendkim](http://www.opendkim.org/)

Authentication is done against an ldap server.

Please note that it is very important to have an understanding of how your dns should be setup up for all that to work correctly (especially for dkim, spf and dmarc).

## Dependencies
- server
- nginx
- postgresql
- ldapclient
- php

## Variables
### mailserver_max_imap_connections
- default: 50

Maximal amount of simultaneous imap connections to dovecot.

### mailserver_roundcube_max_filesize
- default: '8M'

The maximal attachment filesize for the roundcube webclient.

### mailserver_dhparam_length
- default: 2048

The bitlenght of the dhparam key.

### mailserver_default_pass_scheme
- default: 'CRYPT'

The format in which the password is stored.

### mailserver_roundcube_dbhost
- default: 'localhost'

The host on which to connect to the roundcube database.

### mailserver_ldap_dn
- default: 'userid=mailserver,ou=Readers,{{ ldapclient_base_dn }}'

The user used to connect to the ldap server.

### mailserver_ldap_group
- default: 'cn=mail,ou=Groups,{{ ldapclient_base_dn }}'

The group dn the user has to be part of to pass validation.

### mailserver_ldap_password

The password used to connect to the ldap server.

### mailserver_id
The id of this mailserver. Only used internally. Must be without spaces.

### mailserver_domain
The mailservers domain as it appears in a mail address after the *@*.

### mailserver_full_domain
The mailservers full domain as one would connect to.

### mailserver_roundcube_domain
The domain where the roundcube webclient is reachable.

### mailserver_postmaster_mail:
The postmaster mail address.

### mailserver_postmaster_password
The password of the postmaster. Create with eg doveadm (use *SHA512-CRYPT*).  
Note that this sets the password only one time.

### mailserver_roundcube_dbpassword
The password used to authenticate to the roundcube database.

### mailserver_roundcube_deskey
Roundcube's deskey, used to encrypt cookies.

### mailserver_roundcube_title
Roundcube's html title.

### mailserver_hostname
Hostname used by dovecot.
