# Backup

## Description
This role takes care of making incremental, deduplicated backups. I use it on the jailhost, so that I am able to pull in the data from the different jails but there is nothing stopping you from using it in the individual jails.

For this purposes it uses [attic](https://attic-backup.org/). Rsync is also around to be used in cronjobs.

## Dependencies
- server

## Variables
### backup_user
- default: 'backup'

The user who stores the backups.

### backup_group
- default: 'backup'

The main group to which the backup user belongs.  
Not really needed at the moment.

### backup_authorized_keys
- default: []

A list of ssh puplic keys, able to access the backup user for pulling the backups.

### backup_paths
- default: []

A list of paths to backup.

Eg:
```
backup_paths:
    - /usr/jails/vanwa_gogs/usr/local/gogs/repositories/
    - /usr/jails/vanwa_gogs/usr/local/pgsql/data/backup/
```
### backup_password

The password used to encrypt the backup.

### backup_keep_hourly
- default: 4

How many hourly backups to keep.

### backup_keep_daily
- default:  7

How many daily backups to keep.

### backup_keep_weekly
- default: 4

How many weekly backups to keep.

### backup_keep_monthly
- default: 6

How many monthly backups to keep.

### backup_cronjobs
- default: []

Cronjob definitions, eg pushing the backups to a remote location.

Each list item consists of an object with the folloing structure:

- name: the cronjob's name
- user: the user who runs the cronjob
- minute: at which minute does the cronjob run
- hour: at which hour does the cronjob run
- day: at which day does the cronjob run
- month: at which month does the cronjob run
- weekday: at which weekday does the cronjob run
- cmd: the command run by the cronjob

Eg:
```
backup_cronjobs:
    -
        name: 'rsync.net push'
        user: "{{ backup_user }}"
        minute: '45'
        hour: '2'
        day: '*'
        month: '*'
        weekday: '*'
        cmd: '/usr/local/bin/rsync -avzH --delete /home/{{ backup_user }}/backup.attic/ 6666666666@rsync.net:my.backup'
```
