# BungeeCord

## Description
Sets up a [BungeeCord](https://www.spigotmc.org/wiki/about-bungeecord/) server to connect multiple [Minecraft](https://minecraft.net/) servers together.

## Dependencies
- server
- ssmtp
- monit

## Variables
### bungeecord_port
- default: 25577

On which port the BungeeCord server listens for connections.

### bungeecord_min_ram
- default: '512M'

The minimal heap space allocation for the JVM.

### bungeecord_max_ram
- default: '2048M'

The maximal heap space allocation for the JVM.

### bungeecord_player_limit
- default: -1

The global player limit for BungeeCord, -1 means no limitation.

### bungeecord_tab_list
- default: GLOBAL_PING

Defines what is shown in the playerlist on the server.

Possible values:
- GLOBAL_PING: shows all players connected to the proxy, complete with ping
- GLOBAL: As GLOBAL_PING above, but without updating their ping
- SERVER: shows the local players on the server you are connected to

### bungeecord_query_enabled
- default: false

Whether to enable UDP query.

### bungeecord_ping_passthrough
- default: false

Whether to pass the ping through when we can reliably get the target server.

### bungeecord_motd

Motd shown in the minecraft client connection screen.

### bungeecord_default_server
- default: 'lobby'

The server that first time players will connect to, or if forcing default server, will always join upon connecting.

### bungeecord_bind_local_address
- default: true

Whether the address Bungee uses to connect to your servers will be explicitly set to the address Bungee is listening on.

### bungeecord_fallback_server
- default: same as default server

Which server to connect to if the default server is unavailable.

### bungeecord_max_players
- default: 0

The max player limit shown in the minecraft client's multiplayer menu. Note that this limit is fake, you can set it to 0, players will still be able to join.

### bungeecord_tab_size
- default: 60

The amount of players that will show up in the playerlist on the server.

### bungeecord_force_default_server
- default: false

If true, the player will always connect to the default server when the join the server. If false, the player will join the server they were last connected to.

### bungeecord_timeout
- default: 30000

How long the BungeeCord proxy should go unresponsive before shutting off all connections.

### bungeecord_connection_throttle
- default: 4000

The time delay before a client is allowed to connect again after a recent connection attempt to prevent attacks.

### bungeecord_ip_forward
- default: true

Whether to enable IP (which forwards the players true IP to Bukkit, rather than the proxy IP) and UUID forwarding (which forwards the players true online-mode UUID to Bukkit, rather than an offline-mode username hash!).

### bungeecord_online_mode
- default: true

Whether the BungeeCord instance will authenticate with the Mojang servers.

### bungeecord_servers
- default: []

A list of servers to connect to from BungeeCord with the following object structure:

- id: id of the minecraft server (this doesn't influence anything)
- motd: motd of the minecraft server
- address: address of the minecraft server
- restricted: if true, prevents players from joining the server unless they have the bungeecord.server.[servername] permission

Eg:
```
bungeecord_servers:
  -
    id: 'vanwa'
    motd: '&1the old world'
    address: 10.0.0.1:25565
    restricted: false
  -
    id: 'lobby'
    motd: '&1Lobby'
    address: 10.0.0.2:25565
    restricted: false
  -
    id: 'creative'
    motd: '&1vanwa creative'
    address: 10.0.0.3:25565
    restricted: false
```
