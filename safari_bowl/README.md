# Safari Bowl
## Description
Creates a [Safari Bowl](https://github.com/milanvanzanten/SafariBowl) instance, a wonderful little game my brother created at university with some other people for a group assignment.

And yes, the inspiration for it was [Blood Bowl](https://en.wikipedia.org/wiki/Blood_Bowl) ;)

## Dependencies
- server
- ssmtp
- monit

## Variables
### safari_bowl_port
- default: 9989

Port on which the Safari Bowl server listens.
