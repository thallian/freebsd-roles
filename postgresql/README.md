# PostgreSQL
## Description
Creates a [PostgreSQL](http://www.postgresql.org/) instance.

Note that the dictionary keys do not matter, the only need to be different between roles.

## Dependencies
- server
- ssmtp
- monit

## Variables
### postgresql_user_group_id
uid/guid of the pgsql user.

**This has to be different for every jail that uses this role!** The reason for this is that PostgreSQL uses shared memory and the uid works as some kind of key.

### postgresql_dbs
- default: {}

A dictionary of databases and users which have to exist with the following object structure:

- owner: the database owner
- password: the database password
- name: the database name

Eg:
```
postgresql_dbs:
    owncloud:
        owner: "{{ owncloud_dbuser }}"
        password: "{{ owncloud_dbpassword }}"
        name: "{{ owncloud_dbname }}
```
