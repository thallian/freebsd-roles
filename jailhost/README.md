# Jailhost
## Description
A role used in addition to the standard `jails_host` role. It sets up a backup configuration, creates zfs mounts, an admin user seperate from root and fail2ban for ssh.

## Dependencies
- backup
- ssmtp
- monit

## Variables
### jailhost_admin_user
The name of the admin user (group will be called the same).

### jailhost_admin_password
The password of the admin user.


### jailhost_allowed_ssh_keys
- default: []

List of ssh public keys, allowed to login as admin on the jailhost.

### jailhost_mounts
- default: []

List of zfs mounts, structured like this:
- name: name of the zfs system
- mountpoint: where to mount the filesystem

Eg:
```
-
  name: 'tank/something'
  mountpoint: '/usr/local/something/something/'
```

Please note that zfs mounts do not get deleted automatically, you have to do it by hand.

### jailhost_semmnu
- default: 512

Maximum number of processes that can have undo operations pending on any given IPC semaphore on the system.

The jailhost needs to be restarted after changing this.

### jailhost_semmns
- default: 1024

 Maximum number of semaphores system-wide.

The jailhost needs to be restarted after changing this.

### jailhost_semmni
- default: 512

Maximum number of semaphore identifiers (i.e., sets).

The jailhost needs to be restarted after changing this.

### jailhost_fail2ban_interface
Defines the interface fail2ban uses.

### jailhost_webdav_mounts
- default: []

List of Webdav shares to be mounted on the jailhost. They can then be ounted into the jails.

It uses the following structure:

- key: unique name of the share
- uid: user id to use
- gid: group id to use
- user: username for authentication
- password: password for authentication
- src: webdav source url

### server_sftp_chroot

The base path to the sftp root. Does not get created, must be a zfs mount.

Can be empty of not used.

### server_sftp_access

A list with the following structure:

- path: path to the sftp jail (must be beneath {{ server_sftp_chroot }})
- user: name of an existing server user
