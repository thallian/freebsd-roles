# Buildbot Master
## Description
Sets up a collection of [Buildbot](http://buildbot.net/) Masters.

## Dependencies
- server
- ssmtp
- nginx
- monit

## Variables
### buildbot_master_masters
- default: []

A list of masters with the following structure:

- id: the master's id (is used as an url slug)
- title: the master's title
- port: port on which this master listens
- http_port: port where the webserver listens
- username: username for the web interface
- password: password for the web interface
- enable_auth: Whether http basic auth is enabled. If true, it looks for a *.htpasswd_masterid* file in the nginx config root. Defaults to False.
- slaves: list of buildslaves (take a look at the buildbot documentation)
- change_sources: list of change sources (take a look at the buildbot documentation)
- schedulers: list of schedulers (take a look at the buildbot documentation)
- build_steps: list of build steps (take a look at the buildbot documentation)
- builders: list of builders (take a look at the buildbot documentation)
- mime_types: list of additional mime types with the following structure:
    - extension: the file extension
    - mime_type: the mime tpe to this file extension

eg:

```
buildbot_master_masters:
    -
        id: 'sing_alongs'
        title: 'Sing Alongs'
        port: 99999
        http_port: 99998
        username: 'user'
        password: 'password'
        slaves:
            - 'buildslave.BuildSlave("FreeBSD 10.2 amd64", "pass")'
        change_sources:
            - 'changes.GitPoller("https://code.vanwa.ch/r/~sebastian/sing-alongs.git", workdir="gitpoller-workdir", branch="master", pollinterval=300)'
        schedulers:
            - 'schedulers.SingleBranchScheduler(name="all", change_filter=util.ChangeFilter(branch="master"), treeStableTimer=None, builderNames=["buildall"])'
            - 'schedulers.ForceScheduler(name="force", builderNames=["buildall"])'
        build_steps:
            - 'steps.Git(repourl="https://code.vanwa.ch/r/~sebastian/sing-alongs.git", mode="incremental")'
            - 'steps.ShellCommand(command=["make"])'
        builders:
            - 'util.BuilderConfig(name="buildall", slavenames=["FreeBSD 10.2 amd64"], factory=factory)'
        mime_types:
            -
                extension: 'opus'
                mime_type: 'audio/ogg'
            -
                extension: 'midi'
                mime_type: 'audio/midi'
```

### buildbot_master_protocol
- default: 'https'

Protocol where the master instances can be reached. Should be one of:

- http
- https

### buildbot_master_title

The title shown on the landing page.

### buildbot_master_domain

The base domain where the master instances can be reached.
