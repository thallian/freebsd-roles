# PostgreSQL
## Description
Creates a [PostgreSQL](http://www.postgresql.org/) instance.

Note that the dictionary keys do not matter, the only need to be different between roles.

## Dependencies
- server
- ssmtp
- monit

## Variables
### mariadb_root_password

Root password for mariadb. Gets set only once.

### mariadb_dbs
- default: {}

A dictionary of databases and users which have to exist with the following object structure:

- owner: the database owner
- password: the database password
- name: the database name

Eg:
```
mariadb_dbs:
    owncloud:
        owner: "{{ owncloud_dbuser }}"
        password: "{{ owncloud_dbpassword }}"
        name: "{{ owncloud_dbname }}
```
