# Minecraft Dynmap
## Description
Adds the [Dynmap](https://www.spigotmc.org/resources/dynmap.274/) to a minecraft jail.

Please note that the dynmap variables are missing here as I do not know what most of them do. You will have to take a look into the configuration template of dynmap.

## Dependencies
- server
- nginx
