# Jailproxy
## Description
A crucial part for the web access to jails. It is an nginx server, acting as a reverse proxy and tls terminator.

## Dependencies
- server
- nginx

## Variables
### jailproxy_proxies
- default: []

A list of proxied jails where each object has the following structure:

- domain: The domain to proxy.
- jail_ip: The ip to redirect to.
- max_body_size: The maximal body size nginx accepts (affects the maximal upload size).
- clacks_overhead: Whether to activate [X-Clacks-Overhead](http://www.gnuterrypratchett.com/) header.
- framing: Sets the *X-Frame-Options* header to the defined value. Default is *SAMEORIGIN*.
- disable_buffering: Whether [proxy buffering](http://nginx.org/en/docs/http/ngx_http_proxy_module.html#proxy_buffering) is disabled.
- add_args: Whether *$is_args$args* gets added to the proxy_pass line.
- rewrites: a list of [rewrites](http://nginx.org/en/docs/http/ngx_http_rewrite_module.html) with the following structure (default is []):
    - source: the rewrite match
    - destination: how to rewrite it
    - flag: nothing or one of (default is permanent):
        - last
        - break
        - redirect
        - permanent

Eg:
```
-
  domain: 'www.vanwa.ch'
  jail_ip: '10.0.0.1'
  max_body_size: '10M'
  clacks_overhead: true
  rewrites:
      -
          source: '^/foos(.*)'
          destination: 'https://$server_name/bar'
```
