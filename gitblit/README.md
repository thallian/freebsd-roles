# Gitblit
## Description
Sets up a [Gitblit](http://gitblit.com/) instance.

## Dependencies
- server
- ssmtp
- nginx
- monit

## Variables
### gitblit_cache_repository_list
- default: true

Whether to cache the available repository list at startup.

### gitblit_search_repositories_subfolders
- default: true

Whether to search the repositories folder subfolders for other repositories.

### gitblit_search_recursion_depth
- default: -1

Maximum number of folders to recurse into when searching for repositories (-1 disables the limit altogether).

### gitblit_search_exclusions
- default: ''

List of regex exclusion patterns to match against folders.

### gitblit_submodule_url_patterns
- default: '.*?://github.com/(.*)'

List of regex url patterns for extracting a repository name when locating submodules.

### gitblit_daemon_bind_interface
- default: ''

The interface to which the git daemon binds to. Binds to all interfaces when empty.

### gitblit_daemon_port
- default: 9418

Port for serving the git daemon service. Zero disables it.

### gitblit_ssh_port
- default: 29418

Port for serving the ssh service. Zero disables it.

### gitblit_ssh_bind_interface
- default: ''

The interface to which the ssh service binds to. Binds to all interfaces when empty.

### gitblit_ssh_command_start_threads
- default: 2

Number of threads used to parse a command line submitted by a client over ssh.

### gitblit_enable_git_servlet
- default: true

Whether to allow push/pull over http/https with the JGit servlet.

### gitblit_requires_client_certificate
- default: false

Whether to restrict all git servlet access to those with valid X509 client certificate.

### gitblit_enforce_certificate_validity
- default: true

Whether to enforce date checks on client certificates.

### gitblit_certificate_username_OIDs
- default: 'CN'

List of OIDs to extract from a client certificate DN to map a certificate to an account username.

### gitblit_only_access_bare_repositories
- default: false

Whether to only serve/display bare repositories.

### gitblit_accepted_push_transports
- default: 'HTTPS SSH'

List of acceptable transports for pushes. All are acceptable if empty.

Possible values:
- GIT
- HTTP
- HTTPS
- SSH

### gitblit_allow_create_on_push
- default: true

Whether to allow an authenticated user to create a destination repository on a push.

### gitblit_allow_anonymous_pushes
- default: false

Whether to allow anonymous pushes on a global level.

### gitblit_default_access_restriction
- default: 'PUSH'

Default access restriction for new repositories.

One of:
- NONE: anonymous view, clone, & push
- PUSH: anonymous view & clone and authenticated push
- CLONE: anonymous view, authenticated clone & push
- VIEW: authenticated view, clone, & push

### gitblit_default_authorization_control
- default: 'NAMED'

Default authorization control for new repositories.

One of:
- AUTHENTICATED: any authenticated user is granted restricted access
- NAMED: only named users/teams are granted restricted access

### gitblit_user_repository_prefix
- default: '~'

The prefix for a users personal repository directory.

### gitblit_default_incremental_push_tag_prefix
- default: 'r'

The default incremental push tag prefix.

### gitblit_create_repositories_shared
- default: false

Whether new repositories should be created with 'git init --shared'.

Valid values are the values available for the '--shared' option or false.

### gitblit_enable_garbage_collection
- default: false

Whether to enable JGit-based garbage collection. (!!EXPERIMENTAL!!)

### gitblit_garbage_collection_hour
- default: 0

Hour of the day for the GC Executor to scan repositories.

### gitblit_default_garbage_collection_threshold
- default: '500k'

Default minimum total filesize of loose objects to trigger early garbage collection.

### gitblit_default_garbage_collection_period
- default: 7

Default period, in days, between GCs for a repository.

### gitblit_enable_mirroring
- default: false

Whether to automatically fetch ref updates for a properly configured mirror.

### gitblit_mirror_period
- default: '30 mins'

Period between update checks for mirrored repositories.

### gitblit_packed_git_window_size
- default: '8k'

Number of bytes of a pack file to load into memory in a single read operation.

### gitblit_packed_git_limit
- default: '10m'

Maximum number of bytes to load and cache in memory from pack files.

### gitblit_delta_base_cache_limit
- default: '10m'

Maximum number of bytes to reserve for caching base objects that multiple deltafied objects reference.

### gitblit_packed_git_open_files
- default: 128

Maximum number of pack files to have open at once.

### gitblit_packed_git_mmap
- default: false

When true, JGit will use mmap() rather than malloc()+read() to load data from pack files.

### gitblit_check_received_objects
- default: true

Whether to validate all received (pushed) objects.

### gitblit_check_referenced_objects_reachable
- default: true

Whether to validate if all referenced but not supplied objects are reachable.

### gitblit_max_object_size_limit
- default: 0

The maximum allowed Git object size. Zero disables maximum object size checking.

### gitblit_max_pack_size_limit
- default: -1

The maximum allowed pack size. -1 disables maximum pack size checking.

### gitblit_accept_new_tickets
- default: true

Globally enable or disable creation of new bug, enhancement, task, etc tickets for all repositories.

### gitblit_accept_new_patchsets
- default: true

Globally enable or disable pushing patchsets to all repositories.

### gitblit_require_approval
- default: false

Whether patchsets must have an approval score to enable the merge button.

### gitblit_close_on_push_commit_message_regex
- default: '(?:fixes|closes)[\\s-]+#?(\\d+)'

The case-insensitive regular expression used to identify and close tickets on push.

### gitblit_tickets_per_page
- default: 25

Number of tickets to display on a page.

### gitblit_plugins_registry
- default: 'http://plugins.gitblit.com/plugins.json'

Url to the registry of available plugins.

### gitblit_default_thread_pool_size
- default: 2

Number of threads used to handle miscellaneous tasks in the background.

### gitblit_authenticate_view_pages
- default: false

Whether to require authentication to see everything but the admin pages.

### gitblit_enforce_http_basic_authentication
- default: false

Whether to require a client-side basic authentication prompt instead of the standard form-based login.

### gitblit_authenticate_admin_pages
- default: true

Whether to require admin authentication for the admin functions and pages.

### gitblit_allow_cookie_authentication
- default: true

Whether to allow Gitblit to store a cookie in the user's browser for automatic authentication.

### gitblit_allow_deleting_non_empty_repositories
- default: true

Whether to allow deletion of non-empty repositories.

### gitblit_include_personal_repositories_in_list
- default: false

Whether to include personal repositories in the main repositories list.

### gitblit_authentication_providers
- default: ''

Ordered list of external authentication providers which will be used if authentication against the local user service fails.

Possible values:
- htpasswd
- ldap
- pam
- redmine
- salesforce
- windows

### gitblit_password_storage
- default: 'md5'

How to store passwords.

One of:

- clear
- md5
- combined-md5

### gitblit_min_password_length
- default: 5

Minimum valid length for a plain text password.

### gitblit_header_background_color
- default: ''

Custom header background CSS color.  If unspecified, the default color will be used.

### gitblit_header_foreground_color
- default: ''

Custom header foreground CSS color.  If unspecified, the default color will be used.

### gitblit_header_hover_color
- default: ''

Custom header hover CSS color.  If unspecified, the default color will be used.

### gitblit_header_border_color
- default: ''

Custom header border CSS color.  If unspecified, the default color will be used.

### gitblit_header_border_focus_color
- default: ''

Custom header border focus CSS color.  If unspecified, the default color will be used.

### gitblit_allow_administration
- default: true

Whether users with "admin" role can create repositories, create users, and edit repository metadata.

### gitblit_hide_header
- default: false

Whether to disable rendering of the top-level navigation header.

### gitblit_enable_rpc_servlet
- default: true

Whether the rpc servlet is enabled.

### gitblit_enable_rpc_management
- default: false

Whether to allows rpc clients to manage repositories and users of the Gitblit instance.

### gitblit_enable_rpc_administration
- default: false

Whether to allows rpc clients to control the server settings and monitor the health of the Gitblit instance.

### gitblit_page_cache_expires
- default: 0

Number of minutes to cache a page in the browser since the last request.

### gitblit_use_responsive_layout
- default: true

Whether the web ui layout will respond and adapt to the browser's dimensions.

### gitblit_allow_gravatar
- default: false

Whether to allow Gravatar images to be displayed in Gitblit pages.

### gitblit_allow_zip_downloads
- default: true

Whether to allow dynamic zip downloads.

### gitblit_compressed_downloads
- default: 'zip gz'

List of formats that will be displayed to download compressed archive links.

Possible values:
- zip
- tar
- gz
- xz
- bzip2

### gitblit_allow_lucene_indexing
- default: true

Whether to allow optional Lucene integration.

### gitblit_lucene_frequency
- default: '2 mins'

Frequency of Lucene repository indexing.

### gitblit_allow_forking
- default: true

Whether to allow an authenticated user to create forks of a repository.

### gitblit_short_commit_id_length
- default: 6

Length of shortened commit hash ids.

### gitblit_allow_flash_clipboard
- default: false

Whether to use Clippy (Flash solution) to provide a copy-to-clipboard button.

### gitblit_max_activity_commits
- default: 0

Default maximum number of commits that a repository may contribute to the activity page, regardless of the selected duration.

### gitblit_syndication_entries
- default: 25

Default number of entries to include in RSS Syndication links.

### gitblit_show_repository_sizes
- default: true

Whether to show the size of each repository on the repositories page.

### gitblit_show_federation_registrations
- default: false

Whether to show federation registrations (without token) and the current pull status to non-administrator users.

### gitblit_login_message
- default: 'gitblit'

The message displayed on login. Specifying "gitblit" uses the internal welcome message.

### gitblit_repositories_message
- default: 'gitblit'

The message displayed above the repositories table. Specifying "gitblit" uses the internal welcome message.

### gitblit_blob_encodings
- default: 'UTF-8 ISO-8859-1'

Ordered list of charsets/encodings to use when trying to display a blob. The server's default charset is always appended to the encoding list.

### gitblit_timezone
- default: "{{ server_timezone }}"

Default timezone to use. Specifying a blank value will default to the JVM timezone.

### gitblit_use_client_timezone
- default: false

Whether to use the client timezone when formatting dates.

### gitblit_time_format
- default: 'HH:mm'

Time format.

### gitblit_datestamp_short_format
- default: 'yyyy-MM-dd'

Short date format.

### gitblit_datestamp_long_format
- default: 'EEEE, MMMM d, yyyy'

Long date format.

### gitblit_datetimestamp_long_format
- default: 'EEEE, MMMM d, yyyy HH:mm Z'

Long timestamp format.

### gitblit_mount_parameters
- default: true

Whether to use pretty or parameter URLs.

### gitblit_allow_app_clone_links
- default: true

Whether app-specific clone links are displayed.

### gitblit_repository_list_type
- default: 'grouped'

How to present the repositories list.

One of:
- grouped: group nested/subfolder repositories together (no sorting)
- flat: flat list of repositories (sorting allowed)

### gitblit_repository_root_group_name
- default: 'main'

If using a grouped repository list and there are repositories at the root level show this as group name.

### gitblit_repository_list_swatches
- default: true

Whether to display the repository swatch color next to the repository name link in the repositories list.

### gitblit_commit_message_renderer
- default: 'plain'

Default commit message renderer.

One of:
- plain
- markdown

### gitblit_show_email_addresses
- default: true

Whether email addresses are shown in the web ui.

### gitblit_show_search_type_selection
- default: false

Whether to show a combobox in the page links header with commit, committer, and author search selection.

### gitblit_generate_activity_graph
- default: false

Whether to display activity graphs on the dashboard, activity, and summary pages. Makes use of the external Google Charts API.

### gitblit_show_branch_graph
- default: true

Whether to show the commits branch graph in the summary page and commits/log page.

### gitblit_activity_duration
- default: 7

Default number of days to show on the activity page.

### gitblit_activity_duration_choices
- default: '1 3 7 14 21 28'

Choices for days of activity to display.

### gitblit_activity_duration_maximum
- default: 30

Maximum number of days of activity that may be displayed on the activity page.

### gitblit_activity_cache_days
- default: 14

The number of days of commits to cache in memory for the dashboard, activity and project pages. Zero disables all caching.

### gitblit_metric_author_exclusions
- default: ''

Case-insensitive list of authors to exclude from metrics.

### gitblit_summary_commit_count
- default: 16

The number of commits to display on the summary page.

### gitblit_summary_refs_count
- default: 5

The number of tags/branches to display on the summary page.

### gitblit_summary_show_readme
- default: true

Whether to a README file, if available, on the summary page.

### gitblit_items_per_page
- default: 50

The number of items to show on a page before showing the first, prev, next pagination links.

### gitblit_overview_reflog_count
- default: 5

The number of reflog changes to display on the overview page.

### gitblit_reflog_changes_per_page
- default: 10

The number of reflog changes to show on a reflog page before show the first, prev, next pagination links.

### gitblit_tab_documents
- default: 'readme home index changelog contributing submitting_patches copying license notice authors'

Names of documents in the repository root to be displayed in tabs on the repository docs page.

### gitblit_lucene_ignore_extensions
- default: '7z arc arj bin bmp dll doc docx exe gif gz jar jpg lib lzh odg odf odt pdf ppt pptx png so swf tar xcf xls xlsx zip'

Registered file extensions to ignore during Lucene indexing.

### gitblit_pretty_print_extensions
- default: 'aea agc basic c cbm cl clj cpp cs css dart el erl erlang frm fs go groovy h hpp hs htm html java js latex lisp ll llvm lsp lua ml moxie mumps n nemerle pascal php pl pm prefs properties proto py r R rb rd Rd rkt s S scala scm sh Splus sql ss tcl tex vb vbs vhd vhdl wiki xml xq xquery yaml yml ymlapollo'

Registered extensions for google-code-prettify.

### gitblit_markdown_extensions
- default: 'md mkd markdown MD MKD'

Registered extensions for markdown transformation.

### gitblit_mediawiki_extensions
- default: 'mw mediawiki'

Registered extensions for mediawiki transformation.

### gitblit_twiki_extensions
- default: 'twiki'

Registered extensions for twiki transformation.

### gitblit_textile_extensions
- default: 'textile'

Registered extensions for textile transformation.

### gitblit_confluence_extensions
- default: 'confluence'

Registered extensions for confluence transformation.

### gitblit_tracwiki_extensions
- default: 'tracwiki'

Registered extensions for tracwiki transformation.

### gitblit_image_extensions
- default: 'bmp jpg jpeg gif png ico'

Image extensions.

### gitblit_binary_extensions
- default: '7z arc arj bin dll doc docx exe gz jar lib lzh odg odf odt pdf ppt pptx so tar xls xlsx zip'

Registered extensions for binary blobs.

### gitblit_aggressive_heap_management
- default: false

Aggressive heap management will run the garbage collector on every generated page.

### gitblit_debug_mode
- default: false

Whether to run the web app in debug mode.

### gitblit_force_default_locale
- default: ''

Forces a default locale for all users, ignoring the browser's settings if set.

### gitblit_mail_host

Smtp host to use for email sending.

### gitblit_mail_port
- default: 587

Port to use for smtp requests

### gitblit_mail_debug
- default: false

Whether to run the mail executor in debug mode.

### gitblit_mail_smtps
- default: false

Whether to use smtps.

### gitblit_mail_starttls
- default: true

Whether to use starttls for smtp.

### gitblit_mail_user

Username to authenticate to the smtp server.

### gitblit_mail_password

Password to authenticate to the smtp server.

### gitblit_mail_from_address

From address for sent emails.

### gitblit_admin_addresses
- default: ''

List of email addresses for the Gitblit administrators.

### gitblit_federation_name
- default: ''

The name used for federation status acknowledgments.

### gitblit_federation_password
- default: ''

Passphrase for the Gitblit instance. Leaving it empty disables federation.

### gitblit_federation_allow_proposals
- default: false

Whether or not the Gitblit instance can receive federation proposals from another Gitblit instance.

### gitblit_federation_default_frequency
- default: '60 mins'

Default pull frequency if frequency is unspecified on a federation registration.

### gitblit_auto_create_accounts
- default: false

Whether to auto-create user accounts based on the servlet container principal.

### gitblit_windows_allow_guests
- default: false

Whether to allow Windows guest account logins.

### github_windows_permit_built_in_administrators
- default: true

Whether to allow user accounts belonging to the BUILTIN\Administrators group to be Gitblit administrators.

### gitblit_windows_default_domain
- default: ''

The default domain for authentication.

### gitblit_pam_service_name
- default: 'system-auth'

The PAM service name for authentication.

### gitblit_salesforce_org_id
- default: 0

Restrict the Salesforce user to members of this org. Zero means no checks.

### gitblit_ldap_password

The password to connect to the ldap directory if used.

### gitblit_ldap_display_name
- default: 'displayName'

Ldap attribute(s) on the USER record that indicate their display (or full) name.

### gitblit_ldap_email
- default: 'mail'

Ldap attribute(s) on the USER record that indicate their email address.

### gitblit_ldap_uid
- default: 'cn'

Ldap attribute(s) on the USER record that indicate their username to be used in gitblit when synchronizing users.

### gitblit_ldap_synchronize
- default: false

Whether to synchronize all LDAP users and teams into the user service.

### gitblit_ldap_remove_deleted_users
- default: true

Whether to delete non-existent LDAP users from the user service during synchronization.

### gitblit_redmine_url
- default: ''

Url of the redmine instance to authenticate with.

### gitblit_thread_pool_size
- default: 50

Maximum number of concurrent http/https Jetty worker threads to allow.

### gitblit_port
- default: 8080

Http port to serve.

### gitblit_http_bind_interface
- default: ''

The interface to which the http daemon binds to. Binds to all interfaces when empty.

### gitblit_shutdown_port
- default: 8081

Port for shutdown monitor to listen on.

### gitblit_domain

Domain name where the Gitblit instance listens.

### gitblit_min_heap
- default: '128M'

Minimal heap allocation for the JVM.

### gitblit_max_heap
- default: '512M'

Maximal heap allocation for the JVM.
