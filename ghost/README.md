# Etherpad
## Description
Sets up an [Etherpad](http://etherpad.org/) instance.

## Dependencies
- server
- ssmtp
- nginx
- monit

## Variables
### ghost_host
- default: '127.0.0.1'

The host this ghost server binds to.

### ghost_port
- default: '2368'

The port this ghost server listens on.

### ghost_max_body_size
- default: '10M'

The maximal body size nginx accepts (affects the maximal upload size).

### ghost_protocol
- default: 'https'

Which protocol the ghost blog uses.

Must be one of:

- http
- https

### ghost_domain

The domain nginx listens on.

### ghost_smtp_host

The smtp gateway, used for notification mails.

### ghost_smtp_port

The used smtp port.

### ghost_smtp_user

The username for the smtp gateway.

### ghost_smtp_password

The password for the smtp gateway.

### ghost_themes
- default: []

A list of themes to be loaded onto the server

The theme has to be in a folder called *ghost_themes* where ansibles file mechanisms will find it.
