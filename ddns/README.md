**this role is not really tested and I have no clue about either bind or dns**

# DDNS
## Description
Sets up a [Bind](http://www.isc.org/downloads/bind/) server as a [ddns](https://en.wikipedia.org/wiki/Dynamic_DNS) provider, together with [nginx](http://nginx.org/) for easy updating.

Updates to a domain look like this:

```
https://{ddns_update_domain}/cgi-bin/update.cgi?host={dynamic domain}&secret={secret}&ip={ip for dynamic domain}
```

## Dependencies
- server
- ssmtp
- nginx

## Variables
### ddns_base_domain
The dns domain, eg `dyn.vanwa.ch`.

### ddns_update_domain
The domain nginx listens on to update the dynamic domain, eg `ddns.vanwa.ch`.

### ddns_admin_mail
The email address associated with the ddns base domain.

### ddns_domains
- default: []

A list of dynamic domains handled by this server.

- id: id of the dynamic domain (this doesn't influence anything)
- domain: the actual dynamic domain
- secret: the secret used to update the dynamic domain

Eg:
```
ddns_domains:
  -
    id: 'home'
    domain: 'home.dyn.vanwa.ch'
    secret: 'youwish'
```
