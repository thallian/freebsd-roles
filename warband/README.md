# Mount & Blade warband
## Description
Sets up a [Mount & Blade Warband](https://www.taleworlds.com/en/Games/Warband) server.

You need the [dedicated server](https://www.taleworlds.com/en/Games/Warband/Download) files in a folder *files/warband/* in your configuration root. This is needed as it is not allowed to distribute the server files other than from Taleworlds website.

## Dependencies
- server
- ssmtp

## Variables
### warband_port
- default: 7240

The port onwhich the Warband server listens.

### warband_modulei
- default: 'Native'

The module that gets loaded by the Warband server.

### warband_game_mode
- default: 'multiplayer_dm'

The game mode the Warband server uses.

### warband_team_point_limit
- default: 300

How many points a team needs in order to win.

### warband_max_players
- default: 32

Maximum amount of players on the server.

### warband_max_players_premium
- default: 32

Maximum amount of premium players on the server

### warband_num_bots_voteable
- default: 20

Amount of bots allowed.

### warband_randomize_factions
- default: 1

Whether to use two random factions out of the faction pool for each round.

### warband_upload_limit
- default: 30000000

The servers upload bandwith limit (in bytes/s).

### warband_add_to_serverlist
- default: 1

Whether to add the server to the official serverlist.

### warband_servername
Name of the server as shown in the serverlist.

### warband_welcome_msg
Welcome message shown to newly joined players.

### warband_adminpass
Administrator password.

### warband_privatepass
Password for premium members.

### warband_maps:
- default: []

A list of maps to be added to the mapcycle.

Eg:
```
warband_maps:
    - mp_la_haye_sainte
    - mp_arabian_harbour
    - mp_arabian_village
```

### warband_factions:
- default: []

A list of factions to be added toi the faction pool.

Eg:
```
warband_factions:
    - fac_britain
    - fac_france
    - fac_prussia
    - fac_russia
    - fac_austria
```
