# UNMAINTAINED

I replaced these roles with docker on rancher, so this repository is no longer maintained 
(note that I did not have problems with FreeBSD, it was just a good opportunity to learn something new).

# FreeBSD Ansible Roles

These roles are meant to be used with [BSDploy](https://github.com/ployground/bsdploy), that is why you'll find nothing firewall specific in it and that is also the reason why the webservers never use tls. Use the jailproxy role as a tls terminator for that.  

Each role runs as a self contained unit, thats why postgre blows up in your face if you do not use a different uid/gid for every pgsql user in every container.  
The reason I am not running only one application per container is that I am slowly moving away from a monolithic system and figuring out how this works best takes time. That way I can keep my sanity (mostly).  

The containers are not meant to be updated, build new ones, migrate the data, point the firewall to the new internal ip and thrash the old stuff (take a look at the `upgrade` command in the [fabfile](https://code.vanwa.ch/summary/server-management*bsdploy-fabfiles.git) repository). The roles should do no harm when run multiple times but they are not really tested that way.

As I build the roles for my use cases, they are not all as configurable as they could be (this is the case especially for the older ones). There is a lot of potential there.

I am quite new to the FreeBSD world (migrated from Debian) and I am fully aware that I might be doing some horrible stuff. If you find something that's going to eat my cat at some point please tell me (if you're in my area I'll gladly pay you a beer or some other beverage of your choice).

## Caveat

Do not use the roles blindly. Read what they are doing, have a good laugh and then go on to build your own.

Here is a list of the ones I am currently using (they are the most tested):

- buildbot_master
- buildbot_slave
- ddns
- etherpad
- ghost
- gitblit
- jailhost
- jailproxy
- koel
- ldapclient
- mailserver
- murmur
- openldap
- owncloud
- php_host
- prosody
- safari_bowl
- static_host
