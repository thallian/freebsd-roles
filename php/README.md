# PHP
## Description
Creates a [PHP](https://secure.php.net/) instance using [php-fpm](http://php-fpm.org/).

Note that the dictionary keys do not matter, the only need to be different between roles.

## Dependencies
- server

## Variables
### php_user
- default: "{{ nginx_user }}"

The user with which the fastcgi process runs.

### php_group
- default: "{{ nginx_user }}"

The group with which the fastcgi process runs.

### php_package_name
- default: 'php5'

The name of the php package that gets installed with pkgng. Needed so that roles like Owncloud can override it with something like *php56*.

### php_pools
- default: {}

A dictionary of php pools with the following structure:

- name: name of the pool
- php_values: a list of php_values to set
    - name: name of the value
    - value: value to set
- php_admin_values: a list of php_admin_values to set
    - name: name of the value
    - value: value to set

Eg:
```
php_pools:
    owncloud:
        name: 'owncloud'
        php_values:
            -
                name: 'always_populate_raw_post_data'
                value: -1
        php_admin_values:
            -
                name: 'memory_limit'
                value: '512M'
            -
                name: 'default_charset'
                value: 'UTF-8'
```
