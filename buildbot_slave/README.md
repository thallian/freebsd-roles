# Buildbot Slave
## Description
Sets up a [Buildbot](http://buildbot.net/) Slave.

## Dependencies
- server
- ssmtp
- monit

### buildbot_slave_master_address

Address (ip or domain) of the buildbot master.

### buildbot_slave_master_port

Port on which the buildbot master listens.

### buildbot_slave_name

The name of the slave.

### buildbot_slave_password

The password with which to authentiate at the buildbot master.

### buildbot_slave_admin

Info string about this slave's administrator.

### buildbot_slave_host

Description of this slave.

### buildbot_slave_dependencies

A list of packages to be installed on the slave.

### buildbot_slave_directories

A list of directories to be created on the slave with the following structure:

- path: the directories path
- owner: the directories owner
- group: the directories group

### buildbot_slave_files

A list of files to be copied onto the slaves with the following structure:

- src: path to the local file
- path: the directories path
- owner: the directories owner
- group: the directories group
