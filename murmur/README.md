# Murmur
## Description
Creates a [Murmur](http://wiki.mumble.info/wiki/Main_Page) instance for Mumble.  

There is the possibility of creating bots that get their audio from an mpd daemon.

## Dependencies
- server
- ssmtp
- monit

## Variables
### murmur_max_bandwith
- default: 48000

maximum bandwidth (in bits per second) clients are allowed to send speech at.

### murmur_welcome_text
- default: 'welcome to murmur!'

welcome message sent to clients when they connect.

### murmur_autoban_attempt
- default: 10

how many attempts in the timeframe defined at *murmur_autoban_timeframe* are needed to trigger an autoban.

### murmur_autoban_timeframe
- default: 120

the timeframe (in seconds) inside which an autoban can be triggered.

### murmur_autoban_time
- default: 300

How long an autoban lasts (in seconds).

### murmur_opus_treshold
-default: 100

Amount of users with Opus support needed to force Opus usage, in percent (0 = always enabled).

### murmur_max_users
- default: 10

Maximum number of concurrent clients allowed.

### murmur_port
- default: 64738

Port to bind TCP and UDP sockets to

### murmur_channel_nesting_limit
- default: 10

Maximum depth of channel nesting.

### murmur_regex_channelname
- default: '[ \\-=\\w\\#\\[\\]\\{\\}\\(\\)\\@\\|]+'

Regular expression used to validate channel names.

### murmur_regex_username
- default: '[-=\\w\\[\\]\\{\\}\\(\\)\\@\\|\\.]+'

Regular expression used to validate user names.

### murmur_allow_html
- default: True

Whether to allow clients to use HTML in messages, user comments and channel descriptions.

### murmur_cert_required
- default: False

Whether only clients which have a certificate are allowed to connect.

### murmur_sendversion
- default: True

Whether clients are sent information about the servers version and operating system.

### murmur_sslname
The base name for the tls files (certificate and key).

### murmur_server_password
Password to join the server.

### murmur_superuser_password
Password for SuperUser.

### murmur_bots
- default: []

A list of murmur bots with the following object structure:

- id: Bot id (used internally).
- script: Name of the bot script (always *mpd-bot.rb* for now).
- port: Port onwhich the mpd daemon listens.
- args: The arguments passed to the script.
- mpc_cmds: A list of arguments to be run by mpc at startup.

Eg:
```
murmur_bots:
  -
    id: 'radiocc'
    script: 'mpd-bot.rb'
    port: 7701
    args: 'localhost 64738 radio-daemon \"password\" \"The Radio CC\" 72000'
    mpc_cmds:
      - 'add http://ogg.theradio.cc/'
  -
    id: 'random_music'
    script: 'mpd-bot.rb'
    port: 7702
    args: 'localhost 64738 music-daemon \"password\" \"Music\" 72000'
    add_music: true
    mpc_cmds:
      - 'update --wait'
      - 'random on'
      - 'repeat on'
      - 'clear'
```

### murmur_root_channel_name
- default: 'Root'

The name of the root channel.
