# OpenLDAP

## Description
Sets up an [OpenLDAP](http://www.openldap.org/) server with [phpLDAPadmin](https://en.wikipedia.org/wiki/PhpLDAPadmin).

## Dependencies
- server
- ssmtp
- nginx
- php
- monit

## Variables
### openldap_root_dn
- default: "cn=Manager,{{ ldapclient_base_dn }}  

The admin user dc.

### openldap_server_port
- default: 389  

The port on which openldap listens.

### openldap_domain

The domain where phpLDAPadmin is reachable.

### openldap_root_password

The root password for openldap as a SHA512 hash.

Can be created with slappasswd: `slappasswd -h '{SHA512}' -o module-path=/usr/local/libexec/openldap -o module-load=pw-sha2`

### openldap_server_name

The name phpLDAPadmin uses in the overview.

### openldap_base_dc

The base dc used for the seed values.
