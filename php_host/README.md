# Static Host
## Description
Sets up an nginx host serving static files.

## Dependencies
- server
- nginx

## Variables
### static_host_index
- default: 'index.html'

The index file nginx uses.

### static_host_enable_auth
- default: false

Whether http basic auth is enabled. If true, it looks for a *.htpasswd* file in the nginx config root.

### static_host_domain
Domain where the static host is reachable.
