# Minecraft
## Description
Creates a [Minecraft](https://minecraft.net/) instance.

Plugins jar files have to be in a directory *files/minecraft_plugins* in the configuration root and their respective configuration files in the same directory in a subfolder *config*.

## Dependencies
- server
- ssmtp
- monit

## Variables
### minecraft_server_name
The server name, as shown in the connection screen.

### minecraft_motd
The motd, as shown in the connection screen.

### minecraft_port
- default: 25565
On which port the minecraft server listens.

### minecraft_min_ram
- default: '512M'
Minimal heap allocation for the JVM.

### minecraft_max_ram
- default: '2048M'
Maximal heap allocation for the JVM.

### minecraft_query_version
- default: 'HEAD'

The git tag or commit to checkout for the php query API.

### minecraft_online_mode
- default: true

Whether to authenticate connecting players against Mojang servers. Deactivate when behing BungeeCord.

### minecraft_op_permission_level
- default: 4

Sets permission level for ops.

One of:
- 1: Ops can bypass spawn protection.
- 2: Ops can use /clear, /difficulty, /effect, /gamemode, /gamerule, /give, and /tp, and can edit command blocks.
- 3: Ops can use /ban, /deop, /kick, and /op.
- 4: Ops can use /stop.

### minecraft_allow_nether
- default: true

Whether players are allowed to travel through the Nether.

### minecraft_level_name
- default: 'world'

The worlds name as used on disk.

### minecraft_allow_query
- default: false

Whether to allow GameSpy4 protocol queries.

### minecraft_query_port
- default: 25565

On which port the GameSpy4 query listener listens.

### minecraft_allow_flight
- default: false

Whether players are allowed to fly in survival mod with a mod installed for it.

### minecraft_announce_player_achievements
- default: true

Whether to announce player achievements to the world.

### minecraft_level_type
- default: 'DEFAULT'

Determines the type of map that is generated.

One of:
- DEFAULT: Standard world with hills, valleys, water, etc.
- FLAT: A flat world with no features, meant for building.
- LARGEBIOMES: Same as default but all biomes are larger.
- AMPLIFIED: Same as default but world-generation height limit is increased.
- CUSTOMIZED: Same as default unless generator-settings is set to a preset.


### minecraft_enable_rcon
- default: false

Whether to enable remote access to the server console.

### minecraft_force_gamemode
- default: true

Whether to force the set gamemode on players.

### minecraft_spawn_npcs
- default: true

Whether npcs will spawn.

### minecraft_whitelist
- default: true

Whether to use the whitelist for players.

### minecraft_spawn_animals
- default: true

Whether animals will spawn.

### minecraft_hardcore
- default: false

If set to true, players will be permanently banned if they die.

### minecraft_pvp
default: true

Whether to allow pvp.

### minecraft_difficulty
- default: 2

Defines the difficulty (such as damage dealt by mobs and the way hunger and poison affects players) of the server.

One of:
- 0: Peaceful
- 1: Easy
- 2: Normal
- 3: Hard

### minecraft_gamemode
- default: 0

Sets the gameplay mode.

One of:
- 0: Survival
- 1: Creative
- 2: Adventure
- 3: Spectator

### minecraft_player_idle_timeout
- default: 0

If non-zero, players are kicked from the server if they are idle for more than that many minutes.

### minecraft_max_players
- default: 20

The maximum number of players that can play on the server at the same time.

### minecraft_spawn_monsters
- default: true

Whether monsters will spawn.

### minecraft_generate_structures
- default: true

Whether structures (such as villages) will be generated.

### minecraft_view_distance
- default: 8

Sets the amount of world data the server sends the client, measured in chunks in each direction of the player (radius, not diameter).

### minecraft_spawn_protection
- default: 0

Determines the radius of the spawn protection. Setting this to 0 will not disable spawn protection. 0 will protect the single block at the spawn point.

### minecraft_spigot_tab_complete
- default: 0

How many letters are needed for autocomplete to kick in. Set to *-1* to disable.

### minecraft_spigot_log
- default: true

Whether to print player commands to the log.

### minecraft_spigot_spam_exclusions
- default:  
    - '/skill'

**have to find out what this does**

### minecraft_spigot_silent_commandblock_console
- default: false

Whether to allow commandblock output to console.

### minecraft_spigot_replace_commands
- default:  
    - 'setblock'
    - 'summon'
    - 'testforblock'
    - 'tellraw'

List of commands for which to disable the bukkit implementation and use the vanilla ones.

### minecraft_spigot_debug
- default: false

Whether to enable spigot debugging. **find out what this actually means**

### minecraft_spigot_save_user_cache_on_stop_only
- default: false

Whether to constantly save new usercache data to disk or only on server stop.

### minecraft_spigot_timeout_time
- default: 60

How long - in seconds - the server should go unresponsive before performing a thread dump in the console and, if configured, attempt to shut down and restart.

### minecraft_spigot_restart_on_crash
- default: false

Whether to try and restart the server after a crash.

### minecraft_spigot_restart_script
- default: './start.sh'

The script to use for a restart.

### minecraft_spigot_bungeecord
- default: false

Whether the server is behing BungeeCord.

### minecraft_spigot_late_bind
- default: false

Whether to delay players from entering the server until all plugins are loaded.

### minecraft_spigot_sample_count
- default: 12

Controls the amount of (randomly chosen) sample players shown when hovering over the player count in the client's server list.

### minecraft_spigot_player_shuffle
- default: 0

Number of ticks after which the player queue gets shuffled.

### minecraft_spigot_filter_creative_items
- default: true

Whether to disallow vanilla items to spawn.

### minecraft_spigot_user_cache_size
- default: 1000

Controls the size cap of usercache.json.

### minecraft_spigot_int_cache_limit
- default: 1024

Caps the integer cache to the specified value.

### minecraft_spigot_moved_wrongly_threshold
- default: 0.0625

Controls the threshold for the *moved wrongly* check.

### minecraft_spigot_moved_too_quickly_threshold
- default: 100.0

Controls the threshold for the *moved too quickly* check. This is effectively the maximum speed the server will allow a player to move.

### minecraft_spigot_netty_threads
- default: 4

Amount of threads Netty will use to perform networking.

### minecraft_bukkit_allow_end
- default: true

Whether the end is enabled.

### minecraft_bukkit_warn_on_overload
- default: true

Whether the server shows *[WARNING] Can't keep up! Did the system time change, or is the server overloaded?* messages

### minecraft_bukkit_permissions_file
- default: 'permissions.yml'

The name of your permissions file.

### minecraft_bukkit_update_folder
- default: 'update'

The name of the folder to put updated plugins in, which will be moved upon restart.

### minecraft_bukkit_ping_packet_limit
- default: 100

Currently disabled. Previously: How much packets a second the ingame ping list can use, maximum. (integer, packets/sec).

### minecraft_bukkit_use_exact_login_location
- default: false

If true, we will bypass Vanilla's behaviour of checking for collisions and moving the player if needed when they login.

### minecraft_bukkit_plugin_profiling
- default: false

Allows the use of the command /timings. Used to measure time taken by plugin for events.

### minecraft_bukkit_connection_throttle
- default: 4000

The delay before a client is allowed to connect again after a recent connection attempt. Use *0* to deactivate this setting and *-1* if the server is behind BungeeCord.

### minecraft_bukkit_query_plugins
- default: true

Whether the server returns the list of plugins when queried remotely.

### minecraft_bukkit_deprecated_verbose
- default: 'default'

One of:
- true: The server shows warnings when a plugin registers a deprecated event.
- false: The server does not show warnings when a plugin registers a deprecated event.
- default: Always show a warning unless the event in question has been tagged by a developer as not requiring a warning when it is registered.

### minecraft_bukkit_shutdown_message
- default: 'Server closed'

The message displayed to clients when the server stops .

### minecraft_bukkit_spawn_limits
- default:  
    monsters: 70
    animals: 15
    water_animals: 5
    ambient: 15

The maximum amount of the respective creature type allowed to spawn in a chunk.

### minecraft_bukkit_period_in_ticks
- default: 600

The ticks between each chunk garbage collection consideration. If set to 0, chunk gc will be disabled.

### minecraft_bukkit_load_threshold
- default: 0

Number of chunks that need to be loaded since the last garbage collection before considering garbage collection. If set to 0, chunk gc will be disabled.

### minecraft_bukkit_ticks_per
- default:
    animal_spawns: 400
    monster_spawns: 1
    autosave: 6000

Tick delay for the respective feature.

### minecraft_bukkit_aliases
- default: 'now-in-commands.yml'

File in which command aliases can be defined.

### minecraft_plugins
- default: []

A list of plugins to be loaded onto the server with the following object structure:

- filename: The basename of the plugin jarfile. Has to be in a folder called *plugins* where ansibles file mechanisms will find it.
- name: The name of the plugin on the server.
- config: A list of objects with the following structure:
    - src: The configuration template name.
    - dst: The configuration file name on the server.

Eg:
```
  -
    filename: 'ProtocolLib'
    name: 'ProtocolLib'
    config: []
  -
    filename: 'worldedit-bukkit-6.0'
    name: 'WorldEdit'
    config: []
-
  filename: 'CommandBook-2.5'
  name: 'CommandBook'
  config:
    -
      src: commandbook_config.yml.j2
      dest: config.yml
```
