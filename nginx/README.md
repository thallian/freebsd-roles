# Nginx
## Description
Creates an [Nginx](http://nginx.org/) instance with safe defaults.

This role is used extensively in many jails as a reverse proxy.

Note that the dictionary keys do not matter, the only need to be different between roles..

## Dependencies
- server
- ssmtp

## Variables
### nginx_dhparam_required
- default: False

Whether a dhparam is created (only needed when tls is used).  
This can take a long time.

### nginx_dhparam_length
- default: 2048

Length in bits of the dhparam.

### ngins_ssl_protocols
- default: 'TLSv1 TLSv1.1 TLSv1.2'

The enabled tls protocols.

### nginx_ssl_ciphers
- default: 'EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH'

The enabled tls ciphers.

### nginx_ssl_prefer_server_ciphers
- default: 'on'

Whether to prefer the server tls ciphers.

### ngins_ssl_session_cache
- default: 'shared:SSL:10m'

TLS session cache configuration.

### nginx_gzip
- default: 'on'

Whether gzip is enabled in responses.

### nginx_gzip_disable
- default: 'msie6'

A list of clients where gzip should be disabled.

### nginx_vhosts
- default: {}

A dictionary of vhosts on this nginx server. There needs to be a template called <name>_nginx_host.j2 in a vhosts folder.

Eg: vhosts/owncloud_nginx_host.j2

### nginx_upstream_servers
- default: {}

A dictionary of objects with the following structure:

    - name: Name of the upstream server
    - upstream: unix socket or address to connect to

Eg:
```
nginx_upstream_servers:
    nginx:
        name: 'php_owncloud'
        upstream: 'unix:/var/run/php5-fpm-owncloud.sock'
```
