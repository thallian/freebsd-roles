# Prosody
## Description
Creates a [Prosody](https://prosody.im/) instance which authenticates users against an imap server.

## Dependencies
- server
- ssmtp
- nginx

## Variables
### prosody_bosh_port
- default: 5280

The port on which prosodys bosh daemon listens.

### prosody_dbpassword
Password to authenticate to the prosody database.

### prosody_dovecot_auth_host
Imap host to authenticate users against.

### prosody_proxy_domain
Proxy domain for server-proxied file transfers.

### prosody_domains
- default: []

A list of vhosts the prosody instance handles with the following object structure:

- domain: Vhost domain.
- conference_name: MUC name.
- bosh_domain: Domain where bosh is reachable.
- sslname: The base name for the tls files (certificate and key) used for the xmpp vhost. They have to be in a subfolder called certs somehwere where ansibles file mechanisms will find it.
- bosh_sslname: The base name for the tls files (certificate and key) used for the bosh domain. They have to be in a subfolder called certs somehwere where ansibles file mechanisms will find it.
- admin: Admin username.
- insecure_domains: List of domains considered insecure. They are allowed weaker authentication for s2s encryption, like dialback.
- secure_domains; List of domains considered secure. They mus preent a valid certificate for s2s encryption.

Eg:
```
prosody_domains:
  -
    domain: 'vanwa.ch'
    conference_name: 'muc.vanwa.ch'
    bosh_domain: 'bosh.vanwa.ch'
    sslname: 'xmpp.vanwa.ch'
    bosh_sslname: 'bosh.vanwa.ch'
    admin: 'master@vanwa.ch'
    insecure_domains:
      - 'gmail.com'
    secure_domains: []
```
