# Etherpad
## Description
Sets up an [Etherpad](http://etherpad.org/) instance.

## Dependencies
- server
- ssmtp
- nginx
- postgresql
- monit

## Variables
### etherpad_host
- default: '127.0.0.1'

The ip to which the etherpad server binds.

### etherpad_port
- default: '9001'

The port on which the etherpad server listens.

### etherpad_title
- default: 'Etherpad'

The html title.

### etherpad_default_text
- default: 'Welcome to Etherpad!\n\nThis pad text is synchronized as you type, so that everyone viewing this page sees the same text. This allows you to collaborate seamlessly on documents!\n\nGet involved with Etherpad at http:\/\/etherpad.org\n'

The default text in a new pad.

### etherpad_no_colors:
- default: false

Whether to use different colours for different users by default.

### etherpad_show_controls
- default: true

Whether to show textcontrols on a pad by default.

### etherpad_show_chat
- default: true

Whether to show the userchat on a pad by default.

### etherpad_show_linenumbers
- default: true

Whether to show linenumbers on a pad by default.

### etherpad_use_monospace_font
- default: false

Whether to use a monospace font on a pad by default.

### etherpad_user_name
- default: 'unnamed'

The default username for a new user.

### etherpad_user_color
- default: randomly chosen by server

The default colour for a new user.

### etherpad_rtl
- default: false

Whether to use a rtl layout for a pad by default.

### etherpad_always_show_chat
- default: false

Whether to have the userchat always open in a pad by default.

### etherpad_chat_and_users
- default: false

**find out what this does**

### etherpad_language
- default: 'en-gb'

The default pad language.

### etherpad_suppress_errors_in_pad_text
- default: false

Whether to suppress errors from being visible in the default pad text.

### etherpad_require_session
- default: false

Whether users must have a session to access pads. This effectively allows only group pads to be accessed.

### etherpad_edit_only
- default: false

If true, users may edit pads but not create new ones.

### etherpad_session_no_password
- default: false

Whether users who have a valid session, automatically get granted access to password protected pads.

### etherpad_minify
- default: true

If true, all css & js will be minified before being sent to the client.

### etherpad_max_age
- default: 21600

How long may clients use served javascript code (in seconds)? Set to 0 to disable caching.

### etherpad_allow_unknown_file_ends
- default: true

Whether to allow import of file types other than the supported types: txt, doc, docx, rtf, odt, html & htm.

### etherpad_require_authentication
- default: false

Whether to require authentication of all users.

### etherpad_require_authorization
- default: false

Whether to require authorization by a module, or a user with is_admin set.

### etherpad_disable_ip_logging
- default: false

Whether to disable IP logging.

### etherpad_log_level
- default: 'INFO'

Etherpad's loglevel.  
One of:

- DEBUG
- INFO
- WARN
- ERROR

### etherpad_load_test
- default: false

Whether to allow load testing tools to hit the etherpad instance.  
**Warning this will disable security on the instance.***

### etherpad_users
- default: []

Users for basic authentication:

- name: the username
- password: the users password
- is_admin: whether the user is an admin and has access to */admin*
