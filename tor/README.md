# Tor
## Description
Sets up a [Tor](https://www.torproject.org/) node.

## Dependencies
- server
- ssmtp

## Variables
### tor_bandwith
- default: '256 KB'

Throttle traffic to this value.

### tor_burst_bandwith
- default: '512 KB'

Allow burst traffic up to this value.

### tor_exit_policy
- default: 'reject *:*'

This nodes [exit policy](https://trac.torproject.org/projects/tor/wiki/doc/ReducedExitPolicy).

### tor_is_bridge
- default: 0

Whether the node acts as a [Bridge](https://www.torproject.org/docs/bridges.html.en).

### tor_nickname
Nickname of the Tor node.

### tor_contactinfo
The contactinfo of the tornode.
