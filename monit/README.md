# Monit
## Description
A monitoring service: https://mmonit.com/monit/

## Dependencies
- server

## Variables
### monit_check_interval
- default: 30

The interval (in seconds) between checks.

### monit_mailers
- default: []

A list of mails hosts for sending alerts.

It has the following structure:

- host: the host to connect to
- port: the port on which to connect
- user: the username for authentication
- password: the password for authentication
- tls_method: one of `SSLAUTO,SSLV2,SSLV3,TLSV1,TLSV11,TLSV12`, default `TLSV12`
- timeout: connection timeout (in seconds) to the mail host, default is 15

### monit_checks
- default: []

A list of monitoring checks with the following structure:

- type: the service test to perform
- target: the check's name
- args: arguments for this check
- start | stop | restart: what to do at the respective event with the following structure:
    - cmd: the command to run
    - uid: the uid to run the command with (optional)
    - gid: the gid to run the command with (optional)
    - timeout: how long to wait for the command (optional)
- check: modify the default check interval:
    - state: `every` or `not every`
    - cycle: amount of cycles or a cron interval
- if_clauses: a list of if clauses with the following structure (cycles and timeout are mutually exclusive):
    - clause: the test to perform
    - cycles: how many consecutive cycles are needed (optional)
    - timeout: the timeout for the test (optional)
    - consequence: what to to if the clause evaluates to true
- group: the group this check belongs to (optional)
- dependencies: list of dependencies for this check (optional)

Eg:

```
monit_checks:
    -
        type: 'process'
        target: 'nginx'
        args: 'pidfile /var/run/nginx.pid'
        start:
                cmd: '/usr/sbin/service nginx start'
                timeout: 30
        restart:
                cmd: '/usr/sbin/service nginx restart'
                timeout: 30
        if_clauses:
            -
                clause: 'failed host {{ ploy_ip }} port 80'
                timeout: 15
                consequence: 'restart'
            -
                clause: 'failed host {{ ploy_ip }} port 443'
                timeout: 15
                consequence: 'restart'
            -
                clause: '3 restarts within 5 cycles'
                consequence: 'alert'
```

### monit_web_user
- default: 'monitor'

The username for the web interface.

### monit_web_password

The password for the web interface.

### monit_alert_recipient

The email address where alerts are sent to.
