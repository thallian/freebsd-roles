# Mediagoblin
## Description
Creates a [Mediagoblin](http://mediagoblin.org/) instance with support for images, audio, video and pdf.

## Dependencies
- server
- ssmtp
- nginx
- postgresql
- ldapclient
- monit

## Variables
### mediagoblin_exif_visible
- default: true

Whether [exif](https://en.wikipedia.org/wiki/Exchangeable_image_file_format) information is visible for images.

### mediagoblin_dbpassword
The password used to authenticate to the mediagoblin database.

### mediagoblin_version
- default: 'v0.8.1'

The git release tag to checkout.

### mediagoblin_ldap_auth
- default: false

Whether to use ldap authentication. Overrides mediagoblin_allow_registration.

### mediagoblin_ldap_email_field
- default: 'mail'

How the email field in an ldap user record is called.

### mediagoblin_ldap_uid_field
- default: 'cn'

How the username field in an ldap user record is called (the username doesn't allow spaces).

### mediagoblin_ldap_user_field
- default: 'cn'

What field to use in order to look for the user record at registration.

### mediagoblin_domain
The domain where the mediagoblin instance is reachable

### mediagoblin_email_sender
Defines the email address from which email is sent.

### mediagoblin_email_host
The smpt relay host, used to send emails.

### mediagoblin_email_port
The smtp relay port to use.

### mediagoblin_email_user
The username used to authenticate with the smtp relay.

### mediagoblin_email_password
The password used to authenticate with the smtp relay.

### mediagoblin_title
- default: 'Mediagoblin'

The html title of the mediagoblin instance.

### mediagoblin_allow_registration
- default: true

Whether to allow registrations.

### mediagoblin_theme
- default: 'airy'

The theme mediagoblin uses.

One of:
- airy
- sandyseventiesspeedboat

### mediagoblin_welcome
The welcome text of this mediagoblin instance. Allows html.
